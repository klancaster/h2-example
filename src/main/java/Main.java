import java.sql.*;

public class Main {
    public static void main(String[] args) throws SQLException {
        final String JDBC_URL = "jdbc:h2:./target/db;AUTO_SERVER=TRUE";

        Connection connection = DriverManager.getConnection(JDBC_URL,"sa","1234");

        Statement statement = connection.createStatement();
        String sql = "create table mytable (\n" +
                "  id integer not null,\n" +
                "  name varchar(24)\n" +
                ")";

        try {
            statement.execute(sql);
        } catch(Exception exception) {
            System.out.printf("Table already exists");
        }

        String insertData = "insert into mytable values(1,'john')";
        statement.executeUpdate(insertData);
        ResultSet rs = statement.executeQuery("select * from mytable");
        while(rs.next()){
            System.out.println(rs.getString("name"));
        }

    }
}
